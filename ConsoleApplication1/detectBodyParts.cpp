
#include "stdafx.h"
#include "detectBodyParts.h"

using namespace std;
using namespace cv;

String upperbody_cascade_name = "C:/opencv/build/etc/haarcascades/haarcascade_mcs_upperbody.xml";
String lowerbody_cascade_name = "C:/opencv/build/etc/haarcascades/haarcascade_lowerbody.xml";
String fullbody_cascade_name = "C:/opencv/build/etc/haarcascades/haarcascade_fullbody.xml";


// Upper body detection using Haar-like features
void detectUpperBody(Mat frame)
{
	// Load the cascade
	CascadeClassifier upperbody_cascade;
	if (!upperbody_cascade.load(upperbody_cascade_name)) { cout << "Error loading upper body cascade file\n" << endl; return; };

	vector<Rect> upperbody;
	Mat frame_gray;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	// Detect faces
	upperbody_cascade.detectMultiScale(frame_gray, upperbody, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(100, 100));

	for (size_t i = 0; i < upperbody.size(); i++)
	{
		Rect temp = upperbody[i];
		temp.y += 100;
		rectangle(frame, temp, Scalar(255, 255, 0), 4, 8);
	}

	imshow("Upper Body Detection", frame);
}


// Lower body detection using Haar-like features
void detectLowerBody(Mat frame)
{
	// Load the cascade
	CascadeClassifier lowerbody_cascade;
	if (!lowerbody_cascade.load(lowerbody_cascade_name)) { cout << "Error loading lower body cascade file\n" << endl; return; };

	vector<Rect> lowerbody;
	Mat frame_gray;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	// Detect faces
	lowerbody_cascade.detectMultiScale(frame_gray, lowerbody, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(100, 100));

	for (size_t i = 0; i < lowerbody.size(); i++)
	{

		Rect r = lowerbody[i];
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.07);
		r.height = cvRound(r.height*0.8);
		rectangle(frame, r.tl(), r.br(), Scalar(0, 255, 0), 3);
	}

	imshow("Lower Body Detection", frame);
}


// Full body detection using Haar-like features
void detectFullBody(Mat frame)
{
	// Load the cascade
	CascadeClassifier fullbody_cascade;
	if (!fullbody_cascade.load(fullbody_cascade_name)) { cout << "Error loading full body cascade file\n" << endl; return; };

	vector<Rect> fullbody;
	Mat frame_gray;

	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);

	// Detect faces
	fullbody_cascade.detectMultiScale(frame_gray, fullbody, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(100, 100));

	for (size_t i = 0; i < fullbody.size(); i++)
	{
		Rect r = fullbody[i];
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.07);
		r.height = cvRound(r.height*0.8);
		rectangle(frame, r.tl(), r.br(), Scalar(0, 255, 0), 3);
	}

	imshow("Full Body Detection", frame);
}