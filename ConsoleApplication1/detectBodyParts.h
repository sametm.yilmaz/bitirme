
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;


// Upper body detection
void detectUpperBody(Mat frame);

// Lower body detection
void detectLowerBody(Mat frame);

// Full body detection
void detectFullBody(Mat frame);