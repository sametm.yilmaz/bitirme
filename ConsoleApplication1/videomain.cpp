#include "stdafx.h"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/video/tracking.hpp"
#include <vector>
#include <stdio.h>
#include <Windows.h>
#include <iostream>
#include <time.h>
#include <ctime>

using namespace cv;
using namespace std;



int main(int argc, const char** argv)
{
	cout << "olmadi1\n";
	cv::CommandLineParser parser(argc, argv, "{help h||}{@input||}");
	std::string arg = parser.get<std::string>("@input");
	if (arg.empty()) {
		cout << "olmadi2\n";
		return -1;
	}
	// prepare video input
	VideoCapture cap(arg);

	// prepare video output
	VideoWriter outputVideo;

	outputVideo.open("haha.wmv", CV_FOURCC('W', 'M', 'V', '2'), cap.get(CV_CAP_PROP_FPS), Size(640, 480), true);

	// prepare cascadeClassifier
	CascadeClassifier detectorBody;
	CascadeClassifier detectorUpper;
	// !! Put your cascade or opencv cascede into project folder !!
	string cascadeName1 = "C:/opencv/build/etc/haarcascades/haarcascade_fullbody.xml";
	string cascadeName2 = "C:/opencv/build/etc//haarcascades/haarcascade_upperbody.xml";

	// Load cascade into CascadeClassifier
	bool loaded1 = detectorBody.load(cascadeName1);
	bool loaded3 = detectorUpper.load(cascadeName2);





	// Basic video input loop
	for (;;)
	{

		bool Is = cap.grab();
		if (Is == false) {
			cout << "olmadi3\n";
			cout << "Video Capture Fail" << endl;
			break;
		}
		else {
			cout << "olmadi4\n ";
			// Just for measure time   
			const clock_t begin_time = clock();

			// Store results in these 2 vectors
			vector<Rect> human;
			vector<Rect> upperBody;

			// prepare 2 Mat container
			Mat img;
			Mat original;
			cout << "olmadi5\n";
			// capture frame from video file
			cap.retrieve(img, CV_CAP_OPENNI_BGR_IMAGE);
			cout << "olmadi6\n";
			// Resize image if you want with same size as your VideoWriter
			resize(img, img, Size(800, 600));
			cout << "olmadi7\n";
			// Store original colored image
			img.copyTo(original);
			cout << "olmadi8\n ";
			// color to gray image
			cvtColor(img, img, CV_BGR2GRAY);
			cout << "olmadi9\n ";

			// detect people, more remarks in performace section  
			detectorBody.detectMultiScale(img, human, 1.1, 2, 0 | 1, Size(40, 70), Size(80, 300));
			cout << "olmadi10\n ";
			detectorUpper.detectMultiScale(img, upperBody, 1.1, 2, 0 | 1, Size(40, 70), Size(80, 300));
			cout << "olmadi11\n ";


			
			// Draw results from detectorBody into original colored image
			if (human.size() > 0) {
				cout << "olmadi8\n ";
				for (int gg = 0; gg < human.size(); gg++) {

					rectangle(original, human[gg].tl(), human[gg].br(), Scalar(0, 0, 255), 2, 8, 0);

				}
			}


			// Draw results from detectorUpper into original colored image
			if (upperBody.size() > 0) {
				for (int gg = 0; gg < upperBody.size(); gg++) {

					rectangle(original, upperBody[gg].tl(), upperBody[gg].br(), Scalar(255, 0, 0), 2, 8, 0);

				}
			}
			// measure time as current - begin_time
			clock_t diff = clock() - begin_time;
			// convert time into string
			char buffer[126];
			sprintf_s(buffer, "%d", diff);
			// display TIME ms on original image
			putText(original, buffer, Point(100, 20), 1, 2, Scalar(255, 255, 255), 2, 8, 0);
			putText(original, "ms", Point(150, 20), 1, 2, Scalar(255, 255, 255), 2, 8, 0);
			// draw results
			namedWindow("prew", WINDOW_AUTOSIZE);
			imshow("prew", original);
			// make video output
			outputVideo << original;

			int key1 = waitKey(20);

		}
	}
}